package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.MovingViolation;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {
		
	}
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de infracciones en movimiento");
		System.out.println("2. Dar estadisticas diarias de las infracciones");
		System.out.println("3. Dar ultimos n infracciones que terminaron en accidente");
		System.out.println("4. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printDailyStatistics(Queue<VODaylyStatistic> dailyStatistics) {
		System.out.println("Se encontraron "+ dailyStatistics.size() + " elementos");
		for (VODaylyStatistic dayStatistic : dailyStatistics) 
		{
			System.out.println(dayStatistic.getFecha() + " - accidentes:  " + dayStatistic.getCantidadAccidentes()+ ",	infracciones:" +	dayStatistic.getCantidadInfracciones()+ ",	multas totales:	$"+dayStatistic.getFineATM()   );;
		}
	}
	
	public void printMovingViolations(Stack<MovingViolation> violations) {
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (MovingViolation violation : violations) 
		{
			System.out.println(violation.getId() + " " 
								+ violation.getTicketIssuedDate() + " " 
								+ violation.getLocation()+ " " 
								+ violation.getViolationDescription());
		}
	}
	
	public void printMensage(String mensaje) {
		System.out.println(mensaje);
	}
}
