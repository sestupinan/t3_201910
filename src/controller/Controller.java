package controller;

import java.io.FileReader;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.MovingViolation;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;


public class Controller {
 
	public static final String file1 = "./data/Moving_Violations_Issued_in_January_2018_ordered.csv";
	public static final String file2 = "./data/Moving_Violations_Issued_in_February_2018_ordered.csv";
	
	private MovingViolationsManagerView view;
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<MovingViolation> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private Stack<MovingViolation> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
	}
	
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					this.loadMovingViolations();
					break;
					
				case 2:
					Queue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
					view.printDailyStatistics(dailyStatistics);
					break;
					
				case 3:
					view.printMensage("Ingrese el numero de infracciones a buscar");
					int n = sc.nextInt();

					Stack<MovingViolation> violations = this.nLastAccidents(n);
					view.printMovingViolations(violations);
					break;
											
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	

	public void loadMovingViolations() 
	{
		CSVReader reader;
		CSVReader reader2;

		movingViolationsStack = new Stack<MovingViolation>();
		movingViolationsQueue = new Queue<MovingViolation>();

		try 
		{
			
			reader = new CSVReader(new FileReader(file1));
			String [] nextLine = new String[16];
			nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{
				MovingViolation nuevo = new MovingViolation(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), Integer.parseInt(nextLine[10]), nextLine[12], nextLine[13], nextLine[14], nextLine[15]);
				System.out.println("" + nuevo.getId());
				movingViolationsStack.push(nuevo);
				movingViolationsQueue.enqueue(nuevo);
			}
			reader2 = new CSVReader(new FileReader(file2));
			nextLine = reader2.readNext();
			while ((nextLine = reader2.readNext()) != null) 
			{
				MovingViolation nuevo = new MovingViolation(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), Integer.parseInt(nextLine[10]), nextLine[12], nextLine[13], nextLine[14], nextLine[15]);
				System.out.println("" + nuevo.getId());
				movingViolationsStack.push(nuevo);
				movingViolationsQueue.enqueue(nuevo);
			}
			reader.close();
		} 
		catch (Exception e) 
		{
			e  = new Exception("Hubo un error cargando los archivos");
		}
		
	}
	
	public Queue <VODaylyStatistic> getDailyStatistics () 
	{
		Queue <VODaylyStatistic> buscada = new Queue<VODaylyStatistic>();
		Queue <MovingViolation> copia = movingViolationsQueue;
		int cantidadA = 0;
		int cantidadI = 0;
		double fines = 0;
		String fechaAnterior = null;
		while(copia.size()> 0)
		{
			MovingViolation i = copia.dequeue();
			String fechaActual[] = i.getTicketIssuedDate().split("T");
			if(fechaAnterior == null)
			{
				fechaAnterior = fechaActual[0];
				if(i.getAccidentIndicator().equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.getFine();
				cantidadI++;
			}
			else if(fechaActual[0].equals(fechaAnterior)) 
			{
				if(i.getAccidentIndicator().equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.getFine();
				cantidadI++;
			}
			else
			{
				VODaylyStatistic nueva = new VODaylyStatistic(fechaActual[0], cantidadA, cantidadI, fines);
				buscada.enqueue(nueva);
				fechaAnterior = fechaActual[0];
				cantidadA = 0;
				fines = 0;
				cantidadI = 0;
				if(i.getAccidentIndicator().equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.getFine();
				cantidadI++;
			}
		}
		
		return buscada;
	}
	
	public Stack <MovingViolation> nLastAccidents(int n) 
	{
		Stack<MovingViolation> buscada = new Stack<MovingViolation>();
		if(n <= movingViolationsStack.size() )
		{
			for (int i = 0; i < n; i++) 
			{
				buscada.push(movingViolationsStack.pop());
			}
		}

		return buscada;
	}
}
