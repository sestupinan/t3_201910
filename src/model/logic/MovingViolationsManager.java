package model.logic;


import java.io.FileReader;

import com.opencsv.CSVReader;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;


public class MovingViolationsManager {

	Stack<MovingViolation> listStack;
	Queue<MovingViolation> listQueue;

	public void loadMovingViolations(String movingViolationsFile) 
	{
		CSVReader reader;
		listStack = new Stack<MovingViolation>();
		listQueue = new Queue<MovingViolation>();

		try 
		{
			
			reader = new CSVReader(new FileReader(movingViolationsFile));
			String [] nextLine = new String[16];
			while ((nextLine = reader.readNext()) != null) 
			{
				nextLine = reader.readNext();
				MovingViolation nuevo = new MovingViolation(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), Integer.parseInt(nextLine[10]), nextLine[12], nextLine[13], nextLine[14], nextLine[15]);
				listStack.push(nuevo);
				listQueue.enqueue(nuevo);
			}
			reader.close();
		} 
		catch (Exception e) 
		{
			e  = new Exception("Hubo un error cargando los archivos");
		}
		
	}

		
	
	public Queue <VODaylyStatistic> getDailyStatistics ()
	{
		Queue <VODaylyStatistic> buscada = new Queue<VODaylyStatistic>();
		Queue <MovingViolation> copia = listQueue;
		int cantidadA = 0;
		int cantidadI = 0;
		double fines = 0;
		String fechaAnterior = null;
		while(copia.size()> 0)
		{
			MovingViolation i = copia.dequeue();
			String fechaActual = i.getTicketIssuedDate();
			if(fechaActual.equals(fechaAnterior)) 
			{
				if(i.accidentIndicator.equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.fine;
				cantidadI++;
			}
			else if(fechaAnterior == null)
			{
				fechaAnterior = fechaActual;
				if(i.accidentIndicator.equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.fine;
				cantidadI++;
			}
			else
			{
				VODaylyStatistic nueva = new VODaylyStatistic(fechaActual, cantidadA, cantidadI, fines);
				buscada.enqueue(nueva);
				fechaAnterior = fechaActual;
				cantidadA = 0;
				fines = 0;
				cantidadI = 0;
				if(i.accidentIndicator.equals("Yes"))
				{
					cantidadA++;
				}
				fines += i.fine;
				cantidadI++;
			}
		}
		
		return buscada;
	}

	
	public Stack <MovingViolation> nLastAccidents(int n ) {
		
		Stack<MovingViolation> buscada = new Stack<MovingViolation>();
		if(n <= listStack.size() )
		{
			for (int i = 0; i < n; i++) 
			{
				buscada.push(listStack.pop());
			}
		}

		return buscada;
	}	


}
