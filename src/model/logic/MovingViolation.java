package model.logic;


public class MovingViolation 
{
	
	int id;
	String location;
	String address;
	String streetId;
	String xCoordinates;
	String yCoordinates;
	String ticketType;
	int fine;
	int totalPaid;
	int penalty1;
	String accidentIndicator;
	String ticketIssuedDate;
	String violationCode;
	String violationDescription;
	
	public MovingViolation(int pId, String pLocation, String pAddress, String pStreetId, String pXCoordinates, String pYCoordinates, String pTicketType, int pFine, int pTotalPaid, int pPenalty, String pAccidentIndicator, String pTicketIssuedDate, String pViolationCode, String pViolationDescription)
	{
		id = pId;
		location = pLocation;
		address = pAddress;
		streetId = pStreetId;
		xCoordinates = pXCoordinates;
		yCoordinates = pYCoordinates;
		ticketType = pTicketType;
		fine = pFine;
		totalPaid = pTotalPaid;
		penalty1 = pPenalty;
		accidentIndicator = pAccidentIndicator;
		ticketIssuedDate = pTicketIssuedDate;
		violationCode = pViolationCode;
		violationDescription = pViolationDescription;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getStreetId() {
		return streetId;
	}


	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}


	public String getxCoordinates() {
		return xCoordinates;
	}


	public void setxCoordinates(String xCoordinates) {
		this.xCoordinates = xCoordinates;
	}


	public String getyCoordinates() {
		return yCoordinates;
	}


	public void setyCoordinates(String yCoordinates) {
		this.yCoordinates = yCoordinates;
	}


	public String getTicketType() {
		return ticketType;
	}


	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}


	public int getFine() {
		return fine;
	}


	public void setFine(int fine) {
		this.fine = fine;
	}


	public int getPenalty1() {
		return penalty1;
	}


	public void setPenalty1(int penalty1) {
		this.penalty1 = penalty1;
	}


	public String getTicketIssuedDate() {
		return ticketIssuedDate;
	}


	public void setTicketIssuedDate(String ticketIssuedDate) {
		this.ticketIssuedDate = ticketIssuedDate;
	}


	public String getViolationCode() {
		return violationCode;
	}


	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(int totalPaid) {
		this.totalPaid = totalPaid;
	}

	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}

	public String getViolationDescription() {
		return violationDescription;
	}

	public void setViolationDescription(String violationDescription) {
		this.violationDescription = violationDescription;
	}





}
