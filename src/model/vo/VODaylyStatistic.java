package model.vo;

public class VODaylyStatistic 
{
	//TODO
	private String fecha;
	

	private int cantidadAccidentes;
	private int cantidadInfracciones;
	private double fineATM;
	
	public VODaylyStatistic(String pFecha, int pCantidadAccidentes, int pCantidadInfracciones, double pFineATM)
	{
		fecha = pFecha;
		cantidadAccidentes = pCantidadAccidentes;
		cantidadInfracciones = pCantidadInfracciones;
		fineATM = pFineATM;
	}
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getCantidadAccidentes() {
		return cantidadAccidentes;
	}

	public void setCantidadAccidentes(int cantidadAccidentes) {
		this.cantidadAccidentes = cantidadAccidentes;
	}

	public int getCantidadInfracciones() {
		return cantidadInfracciones;
	}

	public void setCantidadInfracciones(int cantidadInfracciones) {
		this.cantidadInfracciones = cantidadInfracciones;
	}

	public double getFineATM() {
		return fineATM;
	}

	public void setFineATM(double fineATM) {
		this.fineATM = fineATM;
	}
}
