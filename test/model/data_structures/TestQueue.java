package model.data_structures;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
public class TestQueue extends TestCase{
	/**
	 * stack sobre el cual se ejecutaran las pruebas
	 */
	private Queue queue;
	
	/**
	 * objetos que se a�adir�n al queue
	 */
	private Object obj1;
	private Object obj2;
	private Object obj3;
	private Object obj4;
	
	@Before
	public void setupEscenario1()
	{
		queue = new Queue();
		queue.enqueue(obj1);
	}
	
	
	/**
     * <b>Prueba:</b> Verifica el m�todo enqueue.<br>
     * <b>M�todos a probar:</b><br>
     * size<br>
     * enqueue<br>
     * <b>Casos de prueba:</b><br>
     * Agrega items exitosamente.
     */
    @Test
	public void testEnqueue()
	{
		assertEquals("La cantidad de items no es correcta", queue.size(),1);
		queue.enqueue(obj2);
		queue.enqueue(obj3);
		queue.enqueue(obj4);
		assertEquals("la cantidad de items no es correcta", queue.size(), 4);
	}
	
	
	/**
     * <b>Prueba:</b> Verifica el m�todo dequeue.<br>
     * <b>M�todos a probar:</b><br>
     * dequeue<br>
     * size<br>
     * <b>Casos de prueba:</b><br>
     * 1. No encuentra un item que no exite.
     * 2. Busca el item exitoamnete.
     * 3. Retira el item del queue exitosamnete.
     */
    @Test
	public void testDequeue()
	{
		queue.dequeue();
		assertEquals("No debi� encontrar el item pues no existe", queue.dequeue(), queue.isEmpty());
		queue.enqueue(obj1);
		queue.enqueue(obj2);
		queue.enqueue(obj3);
		queue.enqueue(obj4);
		
		assertEquals("no retorno el item buscado", queue.dequeue(), obj1);
		Object x = queue.dequeue();
		assertEquals("No tetorn� el item buscado", queue.dequeue(), obj2);
		assertEquals("no retiro el objeto del stack", queue.size(), 3);
		
	}
	
	
	 /**
     * <b>Prueba:</b> Verifica el m�todo peek.<br>
     * <b>M�todos a probar:</b><br>
     * peek<br>
     * size<br>
     * <b>Casos de prueba:</b><br>
     * 1. No encuentra un item que no exite.
     * 2. Busca el item exitosamente.
     * 3. No retira el item del queue exitosamnete.
     */
    @Test
	public void testPeek()
	{
		queue.enqueue(obj2);
		queue.enqueue(obj3);
		queue.enqueue(obj4);
		assertEquals("No debi� encontrar el item pues no existe", queue.peek(), queue.isEmpty());
		assertEquals("No retorn� el item buscado", queue.peek(), obj1);
		Object x = queue.peek();
		assertEquals("No debi� retirar el elemento del stack", queue.peek(), obj1);
		assertEquals("no debio retirar el item del stack", queue.size(), 4);
	}
 
 	@Before
 	public void setUp() throws Exception{
 		System.out.println("Codigo de iniciacion");
 	}
 	
 	@Test
 	public void test() {
 		fail("Not yet implemented");
 	}

}
