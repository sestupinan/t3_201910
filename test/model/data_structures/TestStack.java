package model.data_structures;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import static org.junit.Assert.*;

public class TestStack extends TestCase {
	
	/**
	 * stack sobre el cual se ejecutaran las pruebas
	 */
	private Stack stack;
	
	/**
	 * objetos que se a�adir�n al stack
	 */
	private Object obj1;
	private Object obj2;
	private Object obj3;
	private Object obj4;
	
	@Before
	public void setupEscenario1()
	{
		stack = new Stack();
		stack.push(obj1);
	}
	
	
	/**
     * <b>Prueba:</b> Verifica el m�todo push.<br>
     * <b>M�todos a probar:</b><br>
     * size<br>
     * push<br>
     * <b>Casos de prueba:</b><br>
     * Agrega items exitosamente.
     */
    @Test
	public void testPush()
	{
		assertEquals("La cantidad de items no es correcta", stack.size(),1);
		stack.push(obj2);
		stack.push(obj3);
		stack.push(obj4);
		assertEquals("la cantidad de items no es correcta", stack.size(), 4);
	}
	
    /**
     * <b>Prueba:</b> Verifica el m�todo pop.<br>
     * <b>M�todos a probar:</b><br>
     * pop<br>
     * size<br>
     * <b>Casos de prueba:</b><br>
     * 1. No encuentra un item que no exite.
     * 2. Busca el item exitoamnete.
     * 3. Retira el item del stack exitosamnete.
     */
    @Test
	public void testPop()
	{
    	stack.push(obj2);
		stack.push(obj3);
		stack.push(obj4);
		assertEquals("No debi� encontrar el item pues no existe", stack.pop(), stack.isEmpty());
		assertEquals("no retorno el item buscado", stack.pop(), obj4);
		Object x = stack.pop();
		assertEquals("No tetorn� el item buscado", stack.pop(), obj3);
		assertEquals("no retiro el objeto del stack", stack.size(), 3);
		
	}
    
    /**
     * <b>Prueba:</b> Verifica el m�todo peek.<br>
     * <b>M�todos a probar:</b><br>
     * peek<br>
     * size<br>
     * <b>Casos de prueba:</b><br>
     * 1. No encuentra un item que no exite.
     * 2. Busca el item exitosamente.
     * 3. No retira el item del stack exitosamnete.
     */
    @Test
	public void testPeek()
	{
    	stack.push(obj2);
		stack.push(obj3);
		stack.push(obj4);
		assertEquals("No debi� encontrar el item pues no existe", stack.peek(), stack.isEmpty());
	assertEquals("No retorn� el item buscado", stack.peek(), obj4);
		Object x = stack.peek();
		assertEquals("No debi� retirar el elemento del stack", stack.peek(), obj4);
		assertEquals("no debio retirar el item del stack", stack.size(), 4);
	}
	
	@Before
	public void setUp() throws Exception{
		System.out.println("Codigo de iniciacion");
	}
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}